from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return 'w'

@app.route('/a/<auid>')
def a(auid):
    return '{}'.format(auid)

@app.route('/uuid/<uid>/')
def uuid(uid):
    return render_template('html.html', uidd=uid)

if __name__ == '__main__':
    app.run() #啟動flask
